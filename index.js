"use strict";

// AJAX це технологія, яка дозволяє оновлювати данні та не перезавантажувати
//  веб-сторінку, чим прискорює роботу веб-застосунку

const URL = "https://ajax.test-danit.com/api/swapi/films";

class StarWars {
  getFilms(url) {
    return fetch(url).then((response) => response.json());
  }
  getCharacters(film) {
    return film.characters.map((character) => {
      return fetch(character).then((response) => response.json());
    });
  }
  render(films) {
    const list = document.createElement("ul");

    films.forEach((film) => {
      try {
        const filmItem = document.createElement("li");
        filmItem.innerHTML = ` Episode: ${film.episodeId}  <b>${film.name}</b> <br/> ${film.openingCrawl}`;

        const animation = document.createElement("div");
        animation.classList.add("planet");

        const charactersList = document.createElement("ul");

        Promise.allSettled(this.getCharacters(film))
          .then((result) => {

            animation.style.display = 'none';
            
            result.forEach((element) => {
              const character = document.createElement("li");
              character.textContent = element.value.name;
              charactersList.append(character);
            });
          })
          .catch((error) => {
            console.error(error.message);
          });

        filmItem.append(animation, charactersList);
        list.append(filmItem);
      } catch (error) {
        console.error(error.message);
      }
    });
    return list;
  }
}

const starWars = new StarWars();

const root = document.querySelector("#root");

starWars.getFilms(URL).then((films) => {
  root.append(starWars.render(films));
  
});
